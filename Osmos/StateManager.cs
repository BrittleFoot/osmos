﻿using SFML.Graphics;
using Osmos.Utils;

namespace Osmos
{
    internal class StateManager
    {
        public StateManager(RenderWindow window)
        {
            Window = window;
        }

        public RenderWindow Window { get; private set; }

        public IGameState Current { get; set; }
    }
}