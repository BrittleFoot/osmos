﻿using System;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos
{
    internal class Cursor : CircleShape, IDynamicDrawable
    {
        private bool dravable;
        private Osmos osmos;
        private SmartView view;
        private VideoMode vmode;
        private Window window;

        public Cursor(RenderWindow window, VideoMode vmode, SmartView view, Osmos osmos = null)
        {
            Radius = 50;
            Origin = new Vector2f(50, 50);

            this.window = window;
            this.vmode = vmode;
            this.view = view;
            this.osmos = osmos;

            Enabled = false;

            Texture = new ExtendedTexture("Textures\\crosshair_arrow.tga");
            Texture.Smooth = true;
        }

        public bool Enabled { get; set; }

        public void Update(float time)
        {
            dravable = osmos != null && Enabled && osmos.Player != null && osmos.Player.State != OsmoStates.Dead;

            var scale = view.ScaleAmlount;
            Position = Mouse.GetPosition(window).ToFloat()*scale + view.Center - view.Size/2;
            if (dravable) Rotation = (osmos.Player.Position - Position).Angle()*180/(float) Math.PI - 180;
            Scale = view.Scale;
        }

        public void Draw(RenderWindow window)
        {
            window.SetMouseCursorVisible(!dravable);
            if (dravable)
                window.Draw(this);
        }

        public void SetTarget(Osmos target)
        {
            osmos = target;
        }
    }
}