using System;
using SFML.Graphics;

namespace Osmos
{
    internal class ExtendedTexture : Texture
    {
        private Image image;

        public ExtendedTexture(string filename) : base(filename)
        {
            image = new Image(filename);
            Rotate = 0;
            Pulsationable = false;
        }

        public float Rotate { get; private set; }
        public bool Pulsationable { get; private set; }

        private Color color(double r, double g, double b, double a)
        {
            return new Color((byte) r, (byte) g, (byte) b, (byte) a);
        }

        public ExtendedTexture Rot(float sign)
        {
            Rotate = Math.Sign(sign);
            return this;
        }

        public ExtendedTexture Puls()
        {
            Pulsationable = true;
            return this;
        }

        public ExtendedTexture FromBlackBG(byte outR, byte outG, byte outB)
        {
            for (uint i = 0; i < image.Size.X; i++)
            {
                for (uint j = 0; j < image.Size.Y; j++)
                {
                    var p1 = image.GetPixel(i, j);

                    var R = (double) p1.R/255;
                    var G = (double) p1.G/255;
                    var B = (double) p1.G/255;
                    var Cmax = Math.Max(R, Math.Max(G, B));
                    var Cmin = Math.Min(R, Math.Min(G, B));

                    var outA = Math.Min(p1.A, (Cmax + Cmin)*255/2);

                    image.SetPixel(i, j, color(outR, outG, outB, outA));
                }
            }
            Update(image);
            return this;
        }
    }
}