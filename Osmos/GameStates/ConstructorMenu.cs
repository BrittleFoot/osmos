﻿using System.Linq;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos.GameStates
{
    internal class ConstructorMenu : BaseMenu
    {
        private float size;

        public ConstructorMenu(StateManager stat)
            : base(stat)
        {
            menutext.DisplayedString = "Choose the shape for new OSMOS\nAnd it's size.";
        }

        protected override void LoadMenuitems()
        {
            var c1 = center - new Vector2f(150, 0);
            var c2 = center + new Vector2f(150, 0);

            NameOsmo[menosmos.SpawnOsmo(100, c1).Id] = "Rectangle";
            NameOsmo[menosmos.SpawnOsmo(100, c2).Id] = "Circle";

            var rad = new Vector2f(0, 150);

            for (var i = 0; i < 5; i++)
            {
                NameOsmo[menosmos.SpawnOsmo(30, c1 + rad + new Vector2f(73*i, 0)).Id] = ((2*i + 4)*200).ToString();
            }
        }

        protected override void OnClick(string osmoname, Citizen target)
        {
            if (osmoname == "Back")
            {
                SetState(new GamemodeMenu(state));
            }
            else if (osmoname == "Rectangle")
            {
                if (size > 0)
                {
                    var shape = new RectangleShape(new Vector2f(1, 0.75f)*size);
                    shape.Origin = shape.Size/2;
                    shape.FillColor = COLOR.FIELD;
                    SetState(new ConstructorState(state, shape));
                }
            }
            else if (osmoname == "Circle")
            {
                if (size > 0)
                {
                    var shape = new CircleShape(size/2, 300);
                    shape.Origin = new Vector2f(1, 1)*shape.Radius;
                    shape.FillColor = COLOR.FIELD;
                    SetState(new ConstructorState(state, shape));
                }
            }
            else
            {
                if (target is Osmo)
                {
                    foreach (var osmo in menosmos.AllLittleBastards.Where(one => one is Osmo).Select(one => one as Osmo)
                        )
                        osmo.State = OsmoStates.Little;
                    (target as Osmo).State = OsmoStates.Big;
                }
                size = float.Parse(osmoname);
            }
        }
    }
}