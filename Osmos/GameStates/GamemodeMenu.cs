﻿using SFML.System;
using SFML.Window;

namespace Osmos.GameStates
{
    internal class GamemodeMenu : BaseMenu
    {
        public GamemodeMenu(StateManager stat) : base(stat, false)
        {
            menutext.DisplayedString = "Hello! You are now in OSMOS!\nHave a nice day!";
        }

        protected override void LoadMenuitems()
        {
            NameOsmo[menosmos.SpawnOsmo(100, center - new Vector2f(150, 0)).Id] = "Game";
            NameOsmo[menosmos.SpawnOsmo(100, center + new Vector2f(150, 0)).Id] = "Constructor";
        }

        protected override void OnClick(string osmoname, Citizen target)
        {
            if (osmoname == "Game")
                SetState(new Menu(state));
            if (osmoname == "Constructor")
                SetState(new ConstructorMenu(state));
        }
    }
}