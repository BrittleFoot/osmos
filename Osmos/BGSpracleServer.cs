using System;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos
{
    internal class BGSpracleServer : BaseSparkleServer
    {
        private static Random rnd = new Random();

        private Shape bounds;

        private CircleShape circle;

        public BGSpracleServer(Shape bounds, float spraclesize = 150)
        {
            SpracleSize = spraclesize;
            circle = new CircleShape(SpracleSize);
            circle.Origin = new Vector2f(SpracleSize, SpracleSize);
            circle.Texture = new Texture("Textures\\anim_sparkle.tga");
            circle.Texture.Smooth = true;
            this.bounds = bounds;
        }

        public float SpracleSize { get; set; }

        protected override CircleShape srcCircle
        {
            get { return circle; }
        }

        protected override void SpawnNew()
        {
            if (sprakles.Count < 100 && rnd.Next()%500 == 0)
            {
                if (bounds is RectangleShape)
                {
                    var b = (bounds as RectangleShape).Rect();
                    var x = b.Left + (float) rnd.NextDouble()*b.Width;
                    var y = b.Top + (float) rnd.NextDouble()*b.Height;
                    SpawnSpracle(new Vector2f(x, y), SpracleSize);
                }
                if (bounds is CircleShape)
                {
                    var b = bounds as CircleShape;
                    var bCenter = b.Center();
                    var pos = bCenter +
                              new Vector2f(1, 0).Rotate(rnd.NextDouble()*2*Math.PI)*
                              (SpracleSize + (float) rnd.NextDouble()*(b.Radius - SpracleSize*2));
                    SpawnSpracle(pos, SpracleSize);
                }
            }
        }
    }
}