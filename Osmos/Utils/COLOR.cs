using SFML.Graphics;

namespace Osmos.Utils
{
    public static class COLOR
    {
        public static Color FIELD
        {
            get { return new Color(0, 0, 50); }
        }

        public static Color BACKGROUND
        {
            get { return new Color(0, 0, 20); }
        }
    }
}