﻿using System;
using SFML.Graphics;
using Osmos.Utils;
using SFML.System;

namespace Osmos
{
    internal abstract class Citizen : CircleShape, IDynamicDrawable
    {
        public readonly int Id;

        protected Osmos City;

        public Citizen(Osmos city, Vector2f position = default(Vector2f), float rad = 0)
        {
            Id = GetId();
            City = city;
            City.Add(this);
            Rad = rad;
            Position = position;
            FillColor = new Color(0, 0, 0, 0);
        }

        public Vector2f Speed { get; set; }

        public float Rad
        {
            get { return Radius; }
            set
            {
                Radius = value;
                Origin = new Vector2f(value, value);
            }
        }

        public float Mass
        {
            get { return (float) Math.PI*Rad*Rad; }
            set { Rad = Calcs.MassToRad(value); }
        }

        public abstract string Type { get; }
        public abstract void Update(float time);
        public abstract void Draw(RenderWindow window);

        public bool Contains(Vector2f point)
        {
            return (Position - point).Length() < Rad;
        }

        public void Collision(Shape bounds)
        {
            if (bounds is RectangleShape)
            {
                var R = (bounds as RectangleShape).Rect();
                if (Position.X < R.Left + Rad || Position.X > R.Left + R.Width - Rad)
                {
                    Position = new Vector2f(Position.X < R.Left + Rad ? R.Left + Rad : R.Left + R.Width - Rad,
                        Position.Y);
                    Speed = new Vector2f(-Speed.X, Speed.Y);
                }

                if (Position.Y < R.Top + Rad || Position.Y > R.Top + R.Height - Rad)
                {
                    Position = new Vector2f(Position.X, Position.Y < R.Top + Rad ? R.Top + Rad : R.Top + R.Height - Rad);
                    Speed = new Vector2f(Speed.X, -Speed.Y);
                }
            }
            if (bounds is CircleShape)
            {
                var R = bounds as CircleShape;
                var RCenter = R.Center();


                var vectToOsmo = Position - RCenter;
                vectToOsmo = vectToOsmo + vectToOsmo.Normalize()*Rad;

                var radVector = vectToOsmo.Normalize()*R.Radius;

                if (vectToOsmo.Length() > radVector.Length())
                {
                    Position = RCenter + radVector - vectToOsmo.Normalize()*Rad;
                    var angle = Speed.AngleBetween(vectToOsmo);

                    var alpha = vectToOsmo.Angle();
                    var beta = Speed.Angle();

                    var q2 = Speed.Rotate(alpha - beta).Normalize();
                    var a = q2.X;
                    var b = q2.Y;
                    var sum = a*a + b*b;
                    var s = a/sum;
                    var c = b/sum;

                    var speedQ = new Vector2f(Speed.X*s + Speed.Y*c, Speed.X*-c + Speed.Y*s);
                    speedQ = new Vector2f(-speedQ.X, speedQ.Y);

                    var speedP = new Vector2f(speedQ.X*a - speedQ.Y*b, speedQ.X*b + speedQ.Y*a);

                    Speed = speedP;
                }
            }

            SpecificCollision();
        }

        protected virtual void SpecificCollision()
        {
            // no specific collision :c
        }

        #region Static Fields

        private static int id;

        private static int GetId()
        {
            return id++;
        }

        #endregion
    }
}