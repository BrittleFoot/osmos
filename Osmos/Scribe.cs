using System.Globalization;
using Osmos.Utils;

namespace Osmos
{
    internal static class Scribe
    {
        public static string ToString(Citizen one)
        {
            return string.Format(DateTimeFormatInfo.InvariantInfo, "{0}\t{1}\t{2}", one.Position.String(), one.Rad,
                one is Osmo ? (one as Osmo).State.ToString() : "noosmo");
        }

        public static void SpawnFromString(string line, Osmos osmos)
        {
            var args = line.Split('\t');
            var pos = args[0].ParseVector2f();
            var rad = float.Parse(args[1], DateTimeFormatInfo.InvariantInfo);
            var state = args[2];

            var o = new Osmo(osmos, pos, rad);
            if (state == "Player") osmos.Player = o;
            if (state == "Sun") o.State = OsmoStates.Sun;
        }
    }
}