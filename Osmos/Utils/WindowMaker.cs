﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SFML.Graphics;
using SFML.Window;

namespace Osmos.Utils
{
    internal static class WindowMaker
    {
        public static IEnumerable<VideoMode> Resolutions = VideoMode.FullscreenModes.AsEnumerable();


        public static RenderWindow MakeFast()
        {
            return new RenderWindow(new VideoMode(1024, 768), "Osmos", Styles.Titlebar);
        }

        public static RenderWindow Gui_MakeWindow()
        {
            var result = new DialogRes();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DialogForm(result));

            return result.window;
        }

        #region Console Mode

        public static RenderWindow Console_MakeWindow()
        {
            var vmode = Console_ChooseVideoMode();
            if (vmode.Width == 0) return null;
            var style = Console_ChooseStyle();
            if (style == Styles.None) return null;
            return new RenderWindow(vmode, "Osmos", style);
        }

        private static Styles Console_ChooseStyle()
        {
            var types = new List<string> {"Windowed", "Fullscreen"};

            var res = Chooser.ChooseOne(types, null, v => v, "Window mode");
            switch (res)
            {
                case "Windowed":
                    return Styles.Titlebar;
                case "Fullscreen":
                    return Styles.Fullscreen;
                default:
                    return Styles.None;
            }
        }

        private static VideoMode Console_ChooseVideoMode()
        {
            var result = Chooser.ChooseOne(Resolutions.ToList(), default(VideoMode),
                v => string.Format("{0} x {1}", v.Width, v.Height), "Video Mode");
            return result;
        }

        #endregion
    }
}