﻿using SFML.Graphics;

namespace Osmos
{
    internal static class ColorExt
    {
        public static double Brightness(this Color color)
        {
            return (0.3*color.R + 0.59*color.G + 0.11*color.B)/255;
        }
    }
}