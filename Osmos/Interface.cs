﻿using System.Collections.Generic;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos
{
    internal class Interface : IDynamicDrawable
    {
        public Dictionary<string, Text> Text = new Dictionary<string, Text>();
        private SmartView view;

        public Interface(SmartView view)
        {
            this.view = view;

            Text["UpLeft"] = new Text();
            Text["DownLeft"] = new Text();
            foreach (var txt in Text.Values)
                txt.Font = new Font("Font\\monof55.ttf");
        }


        public void Draw(RenderWindow window)
        {
            foreach (var txt in Text.Values)
                window.Draw(txt);
        }

        public void Update(float time)
        {
            Text["UpLeft"].Position = view.Corner + view.Size.Normalize()*20;
            Text["DownLeft"].Position = view.Corner + new Vector2f(0, view.Size.Y*85/100) +
                                        new Vector2f(view.Size.X, -view.Size.Y).Normalize()*20;

            foreach (var txt in Text.Values)
                txt.Scale = view.Scale;
        }
    }
}