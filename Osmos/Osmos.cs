﻿using System.Collections.Generic;
using System.Linq;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos
{
    internal class Osmos : IDynamicDrawable
    {
        public readonly Shape Bounds;
        private Dictionary<int, Citizen> Citizens = new Dictionary<int, Citizen>();

        private HashSet<int> deadBoys = new HashSet<int>();
        private Osmo player;
        private BGSpracleServer spracleServer;

        public Osmos(Shape bounds)
        {
            Bounds = bounds;
            spracleServer = new BGSpracleServer(Bounds);
        }

        public Osmo Player
        {
            get { return Contains(player) ? player : null; }
            set
            {
                if (player != null) player.State = OsmoStates.Little;
                player = value;
                player.State = OsmoStates.Player;
            }
        }

        public void Update(float time)
        {
            spracleServer.SpracleSize = Player != null ? Player.Rad : 150;
            spracleServer.Update(time);
            foreach (var citizen in AllLittleBastards)
            {
                citizen.Update(time);
                citizen.Collision(Bounds);
            }
            Bury();
        }

        public void Draw(RenderWindow window)
        {
            window.Draw(Bounds);
            spracleServer.Draw(window);
            foreach (var citizen in AllLittleBastards)
            {
                citizen.Draw(window);
            }
        }

        public Osmo SpawnOsmo(float radius, Vector2f position)
        {
            var osm = new Osmo(this, position, radius);
            return osm;
        }

        private void Bury()
        {
            foreach (var boy in deadBoys)
                Citizens.Remove(boy);
            deadBoys.Clear();
        }


        public IEnumerable<string> Snapshot()
        {
            var boundinfo = Bounds is CircleShape
                ? "Circle " + (Bounds as CircleShape).Radius
                : "Rectan " + (Bounds as RectangleShape).Size.String();
            yield return boundinfo;
            foreach (var s in AllLittleBastards.Select(one => Scribe.ToString(one)))
            {
                yield return s;
            }
        }

        #region basic operations with collection of citezens

        public int GetCitizenOn(Vector2f position)
        {
            foreach (var citizen in AllLittleBastards)
            {
                if (citizen.Contains(position)) return citizen.Id;
            }
            return -1;
        }

        public IEnumerable<Citizen> AllLittleBastards
        {
            get
            {
                foreach (var citizen in Citizens.Values)
                    yield return citizen;
            }
        }

        public void Add(Citizen citizen)
        {
            Citizens.Add(citizen.Id, citizen);
        }

        public Citizen Get(int id)
        {
            return Citizens[id];
        }

        public bool Contains(Citizen citizen)
        {
            if (citizen != null)
                return Citizens.ContainsKey(citizen.Id);
            return false;
        }

        public void Kill(Citizen citizen)
        {
            deadBoys.Add(citizen.Id);
        }

        #endregion
    }
}