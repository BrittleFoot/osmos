﻿using System;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos
{
    internal class SmartView : View, IDynamic
    {
        private FloatRect bounds;
        private Cursor cursor;


        private Vector2f newSize;
        private VideoMode vmode;


        public SmartView(VideoMode vmode, Shape bounds)
            : base(default(Vector2f), new Vector2f(vmode.Width, vmode.Height))
        {
            newSize = Size;
            this.vmode = vmode;
            this.bounds = bounds.Rect();
            Moveing = true;
        }

        public Citizen Target { get; set; }
        public bool Moveing { get; set; }

        public Vector2f Corner
        {
            get { return Center - Size/2; }
        }

        public float ScaleAmlount
        {
            get { return Size.Length()/new Vector2f(vmode.Width, vmode.Height).Length(); }
        }

        public Vector2f Scale
        {
            get { return new Vector2f(1, 1)*ScaleAmlount; }
        }


        public void Update(float time)
        {
            if (Moveing)
            {
                if (Target != null && Target.Mass >= 5)
                {
                    var toTarget = Target.Position - Center;
                    Center += toTarget*0.005f*time;
                }
                else if (cursor != null)
                {
                    var toTarget = cursor.Position - Center;
                    Center += toTarget*0.0001f*time;
                }
            }


            if (Center.X < bounds.Left + bounds.Width/4) Center = new Vector2f(bounds.Left + bounds.Width/4, Center.Y);
            if (Center.X > bounds.Left + bounds.Width*3/4)
                Center = new Vector2f(bounds.Left + bounds.Width*3/4, Center.Y);
            if (Center.Y < bounds.Top + bounds.Height/4) Center = new Vector2f(Center.X, bounds.Top + bounds.Height/4);
            if (Center.Y > bounds.Top + bounds.Height*3/4)
                Center = new Vector2f(Center.X, bounds.Top + bounds.Height*3/4);

            var deltaSize = newSize - Size;
            Size += deltaSize*0.005f*time;
        }

        public void SetCursor(Cursor cursor)
        {
            this.cursor = cursor;
        }

        public void Scaling(float amount)
        {
            var delta = newSize.Normalize()*amount;
            var size = newSize + delta;

            var maximumLength = Math.Max(bounds.Height, bounds.Width);
            var maxb = Math.Min(size.X, size.Y);

            var minimumLength = Target != null ? Target.Rad*6f : 0f;

            if ((maxb > minimumLength && amount < 0) || (maxb < maximumLength && amount > 0))
                newSize = size;
        }
    }
}