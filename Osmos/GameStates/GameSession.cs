﻿using System.IO;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos.GameStates
{
    internal class GameSession : IGameState
    {
        private Controller control;
        private Interface face;

        private string filename;
        private Osmos osmos;
        private StateManager state;


        private RenderWindow window;

        public GameSession(StateManager state, string filename)
        {
            this.state = state;
            this.filename = filename;
            window = state.Window;
            osmos = MakeOsmos(filename);
            control = new Controller(window, osmos);
            control.EnableControl();
            face = new Interface(control.View);
        }

        public void Tick(float time)
        {
            time *= control.TimeModifler;

            osmos.Update(time);
            control.Update(time);

            ProccesKeys();

            if (osmos.Player == null || osmos.Player.Mass < 10)
                face.Text["DownLeft"].DisplayedString =
                    "It looks bad.\nPress <Ctrl + R> to restart.\nPress <Ctrl + C> to Exit ti main menu.";
            else
                face.Text["DownLeft"].DisplayedString = "Become the Biggest\n\n<Ctrl + S> to Save progress.";
            face.Update(time);

            window.Clear(COLOR.BACKGROUND);

            osmos.Draw(window);
            face.Draw(window);
            control.Draw(window);
        }

        public Osmos MakeOsmos(string filename)
        {
            var file = File.ReadAllLines("Levels\\" + filename);

            var shapeconfig = file[0].Split();


            Shape shape = null;
            if (shapeconfig[0] == "Rectan")
            {
                shape = new RectangleShape(shapeconfig[1].ParseVector2f());
                shape.Origin = (shape as RectangleShape).Size/2;
            }

            if (shapeconfig[0] == "Circle")
            {
                shape = new CircleShape(float.Parse(shapeconfig[1]), 300);
                shape.Origin = new Vector2f(1, 1)*(shape as CircleShape).Radius;
            }

            shape.FillColor = COLOR.FIELD;
            var osm = new Osmos(shape);

            for (var i = 1; i < file.Length; i++)
            {
                Scribe.SpawnFromString(file[i], osm);
            }

            return osm;
        }

        private void ProccesKeys()
        {
            if (Keyboard.IsKeyPressed(Keyboard.Key.LControl) || Keyboard.IsKeyPressed(Keyboard.Key.RControl))
            {
                if (Keyboard.IsKeyPressed(Keyboard.Key.S))
                {
                    File.WriteAllLines("Levels\\saved " + filename.Replace("saved ", ""), osmos.Snapshot());
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.C))
                {
                    control.DisableControl();
                    state.Current = new GamemodeMenu(state);
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.R))
                {
                    control.DisableControl();
                    state.Current = new GameSession(state, filename);
                }
            }
        }
    }
}