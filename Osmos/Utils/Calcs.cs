﻿using System;

namespace Osmos.Utils
{
    internal static class Calcs
    {
        public static float MassToRad(float mass)
        {
            return (float) Math.Sqrt(mass/Math.PI);
        }
    }
}