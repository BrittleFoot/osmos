﻿using System;
using System.Globalization;
using System.Linq;
using SFML.Window;
using SFML.System;

namespace Osmos.Utils
{
    internal static class VectorsExtentions
    {
        public static float Length(this Vector2f vector)
        {
            return (float) Math.Sqrt(vector.X*vector.X + vector.Y*vector.Y);
        }

        public static Vector2f Normalize(this Vector2f vector)
        {
            return vector.Length() != 0
                ? vector*(1f/vector.Length())
                : new Vector2f(0, 0);
        }

        public static bool LongerThan(this Vector2f vector, Vector2f other)
        {
            return vector.Length() > other.Length();
        }

        public static float DistanceTo(this Vector2f vect, Vector2f other)
        {
            return (vect - other).Length();
        }

        public static float Angle(this Vector2f vect)
        {
            return (float) Math.Atan2(vect.Y, vect.X);
        }

        public static Vector2f ToFloat(this Vector2i vect)
        {
            return new Vector2f(vect.X, vect.Y);
        }


        public static float Multiple(this Vector2f vector, Vector2f other)
        {
            return vector.X*other.X + vector.Y*other.Y;
        }

        public static float AngleBetween(this Vector2f vect, Vector2f other)
        {
            return (float) Math.Acos(Math.Min(vect.Multiple(other)/(vect.Length()*other.Length()), 1));
        }

        public static float DegAngleBetween(this Vector2f vect, Vector2f other)
        {
            return vect.AngleBetween(other)*180/(float) Math.PI;
        }

        public static Vector2f Rotate(this Vector2f v, double radianAngle)
        {
            return new Vector2f((float) (v.X*Math.Cos(radianAngle) - v.Y*Math.Sin(radianAngle)),
                (float) (v.X*Math.Sin(radianAngle) + v.Y*Math.Cos(radianAngle)));
        }

        public static string String(this Vector2f v)
        {
            return string.Format("{0}:{1}", v.X, v.Y);
        }

        public static Vector2f ParseVector2f(this string str)
        {
            var rs = str.Split(':').Select(s => float.Parse(s, DateTimeFormatInfo.InvariantInfo)).ToArray();
            return new Vector2f(rs[0], rs[1]);
        }
    }
}