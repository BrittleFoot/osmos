﻿using System.Collections.Generic;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos.GameStates
{
    internal abstract class BaseMenu : IGameState
    {
        protected RectangleShape bounds;
        protected Controller control;
        protected RectangleShape logo;

        protected Osmos menosmos;
        protected Text menutext;

        private bool mousepressed;

        protected Dictionary<int, string> NameOsmo = new Dictionary<int, string>();

        protected Text textbox;
        protected RenderWindow window;

        public BaseMenu(StateManager state, bool backable = true)
        {
            logo = new RectangleShape();
            logo.Texture = new Texture("Textures\\OsmosLogo.tga");
            logo.Texture.Smooth = true;
            logo.Size = new Vector2f(logo.Texture.Size.X, logo.Texture.Size.Y);

            this.state = state;
            window = state.Window;

            var size = new Vector2f(window.GetVMode().Width, window.GetVMode().Height)*0.9f;
            bounds = new RectangleShape(size);
            bounds.Position -= bounds.Size/2;
            bounds.FillColor = COLOR.FIELD;

            menosmos = new Osmos(bounds);
            control = new Controller(window, menosmos);
            control.EnableControl();
            control.View.Moveing = false;
            ///////////////
            if (backable)
            {
                var pos = bounds.Size/6;
                pos = new Vector2f(pos.Y, pos.X*1.2f);
                var backosmo = menosmos.SpawnOsmo(30, bounds.Position + pos);
                var arrow = new Texture("Textures\\Arrow.png");
                arrow.Smooth = true;
                backosmo.ArtMaster.AddLayer(arrow);
                NameOsmo[backosmo.Id] = "Back";
            }
            LoadMenuitems();
            ///////////////
            textbox = new Text("", new Font("Font\\monof55.ttf"));
            menutext = new Text(textbox);
            menutext.DisplayedString = "";

            state.Window.MouseButtonPressed += MousePressed;
        }

        protected StateManager state { get; }

        protected Vector2f center
        {
            get { return bounds.Position - bounds.Origin + bounds.Size/2; }
        }

        public virtual void Tick(float time)
        {
            window.Clear(COLOR.BACKGROUND);

            menosmos.Update(time);
            control.Update(time);

            logo.Position = bounds.Position;
            menutext.Position = center + new Vector2f(0, bounds.Size.Y)/3;

            var underMouseCitizen = menosmos.GetCitizenOn(control.Cursor.Position);
            if (underMouseCitizen > -1)
            {
                var osmoname = NameOsmo[underMouseCitizen];
                var target = menosmos.Get(underMouseCitizen);
                if (target is Osmo) (target as Osmo).ArtMaster.Eate(true);
                textbox.DisplayedString = osmoname.Split('.')[0];
                textbox.Position = target.Position + new Vector2f(0, target.Radius/2);

                if (mousepressed)
                {
                    OnClick(osmoname, target);
                }
            }
            else
            {
                textbox.DisplayedString = "";
            }
            mousepressed = false;

            menosmos.Draw(window);
            window.Draw(textbox);
            window.Draw(menutext);
            control.Draw(window);
            window.Draw(logo);
        }

        protected void SetState(IGameState state)
        {
            control.DisableControl();
            this.state.Window.MouseButtonPressed -= MousePressed;
            this.state.Current = state;
        }

        protected abstract void LoadMenuitems();
        protected abstract void OnClick(string osmoname, Citizen target);

        private void MousePressed(object sender, MouseButtonEventArgs e)
        {
            if (e.Button == Mouse.Button.Left)
            {
                mousepressed = true;
            }
        }
    }
}