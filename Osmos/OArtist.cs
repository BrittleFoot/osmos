using System;
using System.Collections.Generic;
using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace Osmos
{
    internal class OArtist
    {
        private static Dictionary<OsmoStates, List<ExtendedTexture>> baseset;


        private static ExtendedTexture eatingTexture = new ExtendedTexture("Textures\\BlobInnards.tga").FromBlackBG(
            255, 255, 0);

        public readonly Osmo Host;


        private List<Texture> additionalTextures = new List<Texture>();
        private CircleShape circle;
        private float eating;
        private float puls;

        private float rot;

        static OArtist()
        {
            baseset = new Dictionary<OsmoStates, List<ExtendedTexture>>();

            baseset[OsmoStates.Player] = new List<ExtendedTexture>();

            baseset[OsmoStates.Player].Add(new ExtendedTexture("Textures\\player1.tga").Rot(1));
            baseset[OsmoStates.Player].Add(new ExtendedTexture("Textures\\player2.tga").Rot(-1));
            baseset[OsmoStates.Player].Add(new ExtendedTexture("Textures\\player3.tga"));
            baseset[OsmoStates.Player].Add(new ExtendedTexture("Textures\\player4.tga").Puls()
                .FromBlackBG(200, 200, 200));
            baseset[OsmoStates.Player].Add(new ExtendedTexture("Textures\\player5.tga").FromBlackBG(255, 255, 255));
            baseset[OsmoStates.Player].Add(new ExtendedTexture("Textures\\player6.tga"));
            baseset[OsmoStates.Player].Add(new ExtendedTexture("Textures\\player7.tga"));

            baseset[OsmoStates.Little] = new List<ExtendedTexture>();
            baseset[OsmoStates.Little].Add(new ExtendedTexture("Textures\\simple_small.tga"));

            baseset[OsmoStates.Big] = new List<ExtendedTexture>();
            baseset[OsmoStates.Big].Add(new ExtendedTexture("Textures\\simple_big.tga"));

            baseset[OsmoStates.Sun] = new List<ExtendedTexture>();
            baseset[OsmoStates.Sun].Add(new ExtendedTexture("Textures\\attractor.tga").Rot(+1));
            baseset[OsmoStates.Sun].Add(new ExtendedTexture("Textures\\attractor.tga").Rot(-1));

            baseset[OsmoStates.Dead] = new List<ExtendedTexture>();
        }

        public OArtist(Osmo host)
        {
            Host = host;
            circle = new CircleShape(Host);
        }

        public void AddLayer(Texture texture)
        {
            additionalTextures.Add(texture);
        }

        public void Eate(bool hasFood)
        {
            if (hasFood)
                eating = Math.Min(1, eating + 0.005f);
            else
                eating = Math.Max(0, eating - 0.0005f);
        }

        public void Update(float time)
        {
            circle = new CircleShape(Host);
            circle.FillColor = Color.White;

            rot = (rot + 0.007f*time)%360;
            puls = (float) Math.Pow(Math.Sin(rot*2*Math.PI/180), 2)/3f + 0.5f;
        }

        public void Draw(RenderWindow window)
        {
            // baseset

            foreach (var texture in baseset[Host.State])
            {
                circle.FillColor = Host.State != OsmoStates.Sun ? Color.White : new Color(255, 255, 255, 150);
                circle.Scale = new Vector2f(1.1f, 1.1f);
                circle.Texture = texture;
                circle.TextureRect = new IntRect(0, 0, (int) circle.Texture.Size.X, (int) circle.Texture.Size.Y);
                circle.Rotation = rot*texture.Rotate;
                if (texture.Pulsationable) circle.Scale = new Vector2f(puls, puls);
                window.Draw(circle);
            }

            // specific

            if (eating > 0 && Host.State != OsmoStates.Player)
            {
                circle.Scale = new Vector2f(1.1f, 1.1f);
                circle.FillColor = new Color(255, 255, 255, (byte) (eating*255));
                circle.Texture = eatingTexture;

                window.Draw(circle);
            }


            foreach (var texture in additionalTextures)
            {
                circle.FillColor = new Color(200, 200, 230);
                circle.Texture = texture;
                circle.TextureRect = new IntRect(0, 0, (int) circle.Texture.Size.X, (int) circle.Texture.Size.Y);
                window.Draw(circle, new RenderStates(BlendMode.Add));
            }
        }
    }
}