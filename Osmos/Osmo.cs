﻿using System;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos
{
    internal class Osmo : Citizen
    {
        public OArtist ArtMaster;
        public OsmoStates State = OsmoStates.Little;

        public Osmo(Osmos city, Vector2f position = default(Vector2f), float rad = 0)
            : base(city, position, rad)
        {
            ArtMaster = new OArtist(this);
        }


        public override string Type
        {
            get { return GetType().Name; }
        }

        public void Split(Vector2f target, float part_of_mass)
        {
            var dir = (target - Position).Normalize();

            var son_mass = Math.Max(1, Mass/part_of_mass);
            var new_mass = Mass - son_mass;
            var son_pos = Position + (Calcs.MassToRad(new_mass) + Calcs.MassToRad(son_mass))*dir*1.2f;

            var son = new Osmo(City, son_pos);
            son.Mass = son_mass;
            son.Speed = dir*(0.2f + Speed.Length());

            var impuls = Speed*Mass - son.Speed*son.Mass;

            Mass = new_mass;
            Speed = impuls/Mass;
        }

        private float friction(float time)
        {
            return Math.Max(1 - 0.0000003f*Mass*time, 1 - 0.00001f*time);
        }

        public override void Update(float time)
        {
            if (State == OsmoStates.Sun) Speed = new Vector2f();

            if (Mass < 5 || float.IsNaN(Rad))
            {
                State = OsmoStates.Dead;
                City.Kill(this);
            }
            Position += Speed*time;

            Speed *= friction(time);

            ArtMaster.Update(time);
        }

        public override void Draw(RenderWindow window)
        {
            ArtMaster.Draw(window);
        }

        protected override void SpecificCollision()
        {
            ArtMaster.Eate(false);

            foreach (var citizen in City.AllLittleBastards)
            {
                if (State == OsmoStates.Sun) addVelocity(citizen);
                SolveCollisionWithCitizen(citizen);
            }
        }

        private void addVelocity(Citizen citizen)
        {
            var distance = Position - citizen.Position;
            var d = (Position - citizen.Position).Length();
            var power = Mass*d/(d + 1)/(d + 1);
            citizen.Speed += distance.Normalize()*power*1e-6f;
        }

        protected void SolveCollisionWithCitizen(Citizen citizen)
        {
            if (citizen is Osmo)
            {
                var osmo = citizen as Osmo;
                if (!Equals(osmo) && intersects(osmo) && !(this < osmo))
                {
                    ArtMaster.Eate(true);

                    var delta_rad = Math.Abs(Rad + osmo.Rad - Position.DistanceTo(osmo.Position));
                    var delta_mass = osmo.popRadius(delta_rad);

                    var bigimp = Mass*Speed + delta_mass*osmo.Speed;
                    Mass += delta_mass;

                    Speed = bigimp/Mass;
                }

                if (State == OsmoStates.Player && osmo.State != OsmoStates.Player && osmo.State != OsmoStates.Sun)
                {
                    osmo.State = !(this < osmo) ? OsmoStates.Little : OsmoStates.Big;
                }
            }
        }

        private bool intersects(Osmo osmo)
        {
            return Rad + osmo.Rad > Position.DistanceTo(osmo.Position);
        }

        private float popRadius(float deltarad)
        {
            if (deltarad > 2*Rad - 1)
            {
                var m = Mass;
                Rad = 0.00001f;
                return m;
            }

            var mass = Mass;
            Rad = Math.Abs(Rad - deltarad);
            return mass - Mass;
        }

        #region object overrides

        public override bool Equals(object obj)
        {
            return obj is Osmo ? (obj as Osmo).Id == Id : false;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override string ToString()
        {
            return string.Format("[Osmo] Id:{0} Rad:{1}", Id, Rad);
        }

        #endregion

        #region comparison

        public static bool operator >(Osmo jake, Osmo peter)
        {
            return jake.Rad > peter.Rad;
        }

        public static bool operator <(Osmo jake, Osmo peter)
        {
            return jake.Rad < peter.Rad;
        }

        #endregion
    }
}