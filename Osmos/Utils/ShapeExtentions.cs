using System;
using SFML.Graphics;
using SFML.System;

namespace Osmos.Utils
{
    public static class ShapeExtentions
    {
        public static FloatRect Rect(this RectangleShape shape)
        {
            return new FloatRect(shape.Position.X - shape.Origin.X, shape.Position.Y - shape.Origin.Y, shape.Size.X,
                shape.Size.Y);
        }

        public static Vector2f Center(this CircleShape shape)
        {
            return shape.Position - shape.Origin + new Vector2f(1, 1)*shape.Radius;
        }


        public static FloatRect Rect(this Shape shape)
        {
            if (shape is RectangleShape)
            {
                var s = shape as RectangleShape;
                return s.Rect();
            }
            if (shape is CircleShape)
            {
                var s = shape as CircleShape;
                var corner = s.Position - s.Origin;
                return new FloatRect(corner.X, corner.Y, 2*s.Radius, 2*s.Radius);
            }
            throw new ArgumentException("I dont know other figures!");
        }
    }
}