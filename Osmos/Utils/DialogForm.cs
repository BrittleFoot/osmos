using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SFML.Graphics;
using SFML.Window;

namespace Osmos.Utils
{
    internal class DialogForm : Form
    {
        private Button button;
        private Dictionary<string, VideoMode> res;
        private ComboBox resolutions;
        private DialogRes result;
        private Styles style;

        private ComboBox styles;
        private Dictionary<string, Styles> styls;

        private VideoMode vmode;

        public DialogForm(DialogRes result)
        {
            StartPosition = FormStartPosition.CenterScreen;
            Activate();

            this.result = result;
            DoubleBuffered = true;
            Text = "Osmos configuration";

            res = WindowMaker.Resolutions.ToDictionary(r => string.Format("{0} x {1}", r.Width, r.Height));
            styls = new Dictionary<string, Styles>
            {
                { "Windowed", Styles.Titlebar },
                { "Fullscreen", Styles.Fullscreen }
            };
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MaximizeBox = false;

            resolutions = new ComboBox();
            resolutions.SelectedIndexChanged += ResolutionChanged;
            resolutions.DropDownStyle = ComboBoxStyle.DropDownList;
            resolutions.Parent = this;
            resolutions.Top = Height/2;
            resolutions.Left = Width/4;
            resolutions.Width = Width/2;
            resolutions.Items.AddRange(res.Keys.Cast<object>().ToArray());
            resolutions.SelectedIndex = resolutions.Items.IndexOf("1024 x 768");

            styles = new ComboBox();
            styles.SelectedIndexChanged += StyleItemChanged;
            styles.DropDownStyle = ComboBoxStyle.DropDownList;
            styles.Parent = this;
            styles.Left = Width/4;
            styles.Width = Width/2;
            styles.Top = 2*resolutions.Height + resolutions.Top;
            styles.Items.AddRange(styls.Keys.Cast<object>().ToArray());
            styles.SelectedIndex = styles.Items.IndexOf("Windowed");

            button = new Button();
            button.Parent = this;
            button.Left = Width/6;
            button.Width = 2*Width/3;
            button.Top = resolutions.Top - 6*resolutions.Height;
            button.Height = 4*resolutions.Height;
            button.Text = "PLAY";
            button.MouseClick += button_MouseClick;
        }


        private void button_MouseClick(object sender, MouseEventArgs e)
        {
            result.window = new RenderWindow(vmode, "Osmos", style);
            Close();
        }

        private void StyleItemChanged(object sender, EventArgs e)
        {
            style = styls[(string) styles.SelectedItem] | Styles.Close;
        }

        private void ResolutionChanged(object sender, EventArgs e)
        {
            vmode = res[(string) resolutions.SelectedItem];
        }
    }
}