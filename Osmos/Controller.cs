﻿using System;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;

namespace Osmos
{
    internal class Controller : IDynamicDrawable
    {
        private Osmos osmos;
        private bool rage;

        private RectangleShape vintage;
        private VideoMode vmode;

        private RenderWindow window;

        public Controller(RenderWindow window, Osmos osmos)
        {
            this.window = window;
            this.osmos = osmos;
            vmode = window.GetVMode();
            View = new SmartView(vmode, osmos.Bounds);
            Cursor = new Cursor(window, vmode, View, osmos);
            View.SetCursor(Cursor);

            vintage = new RectangleShape(View.Size);
            vintage.Texture = new Texture("Textures\\vinett.png");
            vintage.FillColor = new Color(0xff, 0xff, 0xff, 0xa0);
        }

        public SmartView View { get; }
        public Cursor Cursor { get; }

        public float TimeModifler { get; private set; }


        public void Draw(RenderWindow window)
        {
            window.Draw(vintage);
            Cursor.Draw(window);
        }

        public void Update(float time)
        {
            if (rage && osmos.Player != null && osmos.Player.State == OsmoStates.Player)
                osmos.Player.Split(Cursor.Position, 1000);

            View.Target = osmos.Player;
            View.Update(time);
            Cursor.Update(time);
            window.SetView(View);
            vintage.Position = View.Corner;
            vintage.Size = View.Size;
        }

        public void EnableControl()
        {
            if (!Cursor.Enabled)
            {
                TimeModifler = 1;
                Cursor.Enabled = true;
                window.MouseButtonPressed += MouseButtonPressed;
                window.MouseButtonReleased += MouseButtonRelease;
                window.MouseWheelMoved += MouseWhell;
                window.KeyPressed += KeyPressed;
            }
        }

        public void DisableControl()
        {
            if (Cursor.Enabled)
            {
                Cursor.Enabled = false;
                window.MouseButtonPressed -= MouseButtonPressed;
                window.MouseButtonReleased -= MouseButtonRelease;
                window.MouseWheelMoved -= MouseWhell;
                window.KeyPressed -= KeyPressed;
            }
        }


        private void MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            if (e.Button == Mouse.Button.Left)
            {
                if (osmos.Player != null)
                    osmos.Player.Split(Cursor.Position, 10);
            }
            if (e.Button == Mouse.Button.Right)
            {
                rage = true;
            }
        }

        private void MouseButtonRelease(object sender, MouseButtonEventArgs e)
        {
            if (e.Button == Mouse.Button.Right)
                rage = false;
        }

        private void MouseWhell(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyPressed(Keyboard.Key.LControl))
            {
                var t = (float) e.Delta;
                var fSqrt2 = (float) Math.Sqrt(2);
                TimeModifler = t > 0 ? Math.Min(4, TimeModifler*fSqrt2) : Math.Max(1f/16, TimeModifler/fSqrt2);
            }
            else
            {
                View.Scaling(-e.Delta*100);
            }
        }


        private void KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.Down)
                Scaleing(+100);
            if (e.Code == Keyboard.Key.Up)
                Scaleing(-100);
        }


        private void Scaleing(float amount)
        {
            View.Scaling(amount);
        }
    }
}