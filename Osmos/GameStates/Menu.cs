﻿using System;
using System.IO;
using System.Linq;
using SFML.Window;
using Osmos.Utils;
using SFML.System;

namespace Osmos.GameStates
{
    internal class Menu : BaseMenu
    {
        public Menu(StateManager stat) : base(stat)
        {
            menutext.DisplayedString = "Choose the Level.";
            
        }


        protected override void LoadMenuitems()
        {
            NameOsmo[-1] = null;
            if (!Directory.Exists("Levels"))
            {
                Directory.CreateDirectory("Levels");
            }
            var files = new DirectoryInfo("Levels").GetFiles().Select(f => f.Name).ToArray();
            if (files.Length == 1)
            {
                NameOsmo[menosmos.SpawnOsmo(100, center).Id] = files[0];
            }
            else
            {
                var rad = new Vector2f(bounds.Size.Y/4, 0);
                var size = (rad - rad.Rotate(2*Math.PI/files.Length)).Length()/3;

                for (var i = 0; i < files.Length; i++)
                {
                    NameOsmo[menosmos.SpawnOsmo(size, center + rad.Rotate(2*Math.PI*i/files.Length)).Id] = files[i];
                }
            }
        }


        protected override void OnClick(string osmoname, Citizen target)
        {
            if (osmoname == "Back")
            {
                SetState(new GamemodeMenu(state));
            }
            else
            {
                SetState(new GameSession(state, osmoname));
            }
        }
    }
}