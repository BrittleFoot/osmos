using System.IO;
using SFML.Graphics;
using SFML.Window;
using Osmos.Utils;

namespace Osmos.GameStates
{
    internal class ConstructorState : IGameState
    {
        private Controller control;

        private Interface face;
        private string LevelName;
        private Osmos osmos;
        private float osmostate;


        private float sizeOfCreated = 6;
        private StateManager state;
        private OsmoStates typeOfCreated = OsmoStates.Little;
        private RenderWindow window;

        public ConstructorState(StateManager state, Shape shape)
        {
            this.state = state;
            window = state.Window;
            osmos = new Osmos(shape);
            control = new Controller(window, osmos);
            control.EnableControl();

            face = new Interface(control.View);
            face.Text["DownLeft"].DisplayedString = "To Save level press <Ctrl + S>.";

            if (!Directory.Exists("Levels"))
            {
                Directory.CreateDirectory("Levels");
            }
            var levelcount = new DirectoryInfo("Levels").GetFiles().Length + 1;
            LevelName = "World " + levelcount;

            window.MouseButtonPressed += window_MouseButtonPressed;
        }

        public void Tick(float time)
        {
            time *= control.TimeModifler;

            ConfigBrush();
            face.Text["UpLeft"].DisplayedString = string.Format("Click to create {0} Osmo with radius {1}.",
                typeOfCreated, (int) sizeOfCreated);
            face.Text["DownLeft"].DisplayedString = "To Save level press <Ctrl + S>.\nYour level name is " + LevelName +
                                                    "\nTo Exit Press <Ctrl + C>.";

            osmos.Update(time);
            face.Update(time);
            control.Update(time);

            window.Clear(COLOR.BACKGROUND);

            osmos.Draw(window);
            face.Draw(window);
            control.Draw(window);
        }

        private void window_MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            if (e.Button == Mouse.Button.Left)
            {
                var o = osmos.SpawnOsmo(sizeOfCreated, control.Cursor.Position);
                o.State = typeOfCreated;
                if (typeOfCreated == OsmoStates.Player) osmos.Player = o;
            }
        }

        public void ConfigBrush()
        {
            if (Keyboard.IsKeyPressed(Keyboard.Key.LControl) || Keyboard.IsKeyPressed(Keyboard.Key.RControl))
            {
                if (Keyboard.IsKeyPressed(Keyboard.Key.S))
                {
                    File.WriteAllLines("Levels\\" + LevelName + ".txt", osmos.Snapshot());
                }
                if (Keyboard.IsKeyPressed(Keyboard.Key.C))
                {
                    control.DisableControl();
                    state.Current = new GamemodeMenu(state);
                }
            }

            if (Keyboard.IsKeyPressed(Keyboard.Key.W))
            {
                sizeOfCreated += 0.1f;
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.S))
            {
                if (sizeOfCreated >= 6)
                    sizeOfCreated -= 0.1f;
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.D))
            {
                osmostate = (osmostate + 0.01f)%5;
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.A))
            {
                if (osmostate - 0.01f < 0)
                    osmostate = 5;
                osmostate = (osmostate - 0.01f)%5;
            }
            typeOfCreated = (OsmoStates) (int) osmostate;
        }
    }
}