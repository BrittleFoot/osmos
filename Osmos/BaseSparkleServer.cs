using System.Collections.Generic;
using SFML.Graphics;
using Osmos.Utils;
using SFML.System;

namespace Osmos
{
    internal abstract class BaseSparkleServer : IDynamicDrawable
    {
        private HashSet<int> deadId = new HashSet<int>();
        protected Dictionary<int, Spracle> sprakles = new Dictionary<int, Spracle>();
        protected abstract CircleShape srcCircle { get; }

        public void Update(float time)
        {
            SpawnNew();

            foreach (var sprakle in sprakles.Values)
                if (!sprakle.Update(time))
                    deadId.Add(sprakle.Id);
            foreach (var id in deadId)
                sprakles.Remove(id);
            deadId.Clear();
        }

        public void Draw(RenderWindow window)
        {
            foreach (var sprakle in sprakles.Values)
            {
                sprakle.Draw(window, srcCircle);
            }
        }

        protected void SpawnSpracle(Vector2f pos, float rad, Vector2f speed = default(Vector2f))
        {
            var spr = new Spracle(pos, rad, speed);
            sprakles.Add(spr.Id, spr);
        }

        protected abstract void SpawnNew();
    }
}