namespace Osmos.Utils
{
    public interface IDynamic
    {
        void Update(float time);
    }
}