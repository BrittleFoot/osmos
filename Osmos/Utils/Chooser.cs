using System;
using System.Collections.Generic;

namespace Osmos.Utils
{
    internal static class Chooser
    {
        public static T ChooseOne<T>(List<T> list, T Default, Func<T, string> toString, string choseType)
        {
            var result = Default;
            var index = 0;
            ConsoleKey key;

            while (true)
            {
                Console.Clear();
                Console.WriteLine("\n\tChoose {0}.\n", choseType);
                for (var i = 0; i < list.Count; i++)
                    Console.WriteLine("{0} {1}", index == i ? ">" : " ", toString(list[i]));
                Console.WriteLine("\n{0} {1}", index == list.Count ? ">" : " ", "Exit");

                key = Console.ReadKey().Key;
                if ((key == ConsoleKey.UpArrow || key == ConsoleKey.W) && index > 0)
                    index--;
                if ((key == ConsoleKey.DownArrow || key == ConsoleKey.S) && index < list.Count)
                    index++;
                if (key == ConsoleKey.Enter)
                {
                    if (index == list.Count)
                        break;
                    result = list[index];
                    break;
                }
            }
            Console.Clear();
            return result;
        }
    }
}