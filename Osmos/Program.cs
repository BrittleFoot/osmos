﻿using System.Diagnostics;
using SFML.Window;
using Osmos.GameStates;
using Osmos.Utils;

namespace Osmos
{
    internal class Program
    {
        private static void AddSfmlToPath() 
            => System.Environment.SetEnvironmentVariable("PATH", System.Environment.GetEnvironmentVariable("PATH") + ";SFML");

        private static void Main(string[] args)
        {
            AddSfmlToPath();

            var window = WindowMaker.Gui_MakeWindow();
            if (window == null) return;

            window.Closed += (s, e) => window.Close();
            window.KeyPressed += (s, e) => { if (e.Code == Keyboard.Key.F4 && e.Alt) window.Close(); };

            var state = new StateManager(window);

            state.Current = new GamemodeMenu(state);


            var clock = new Stopwatch();

            while (window.IsOpen)
            {
                var time = (float) clock.ElapsedTicks/1000;
                clock.Restart();

                window.DispatchEvents();
                window.Clear();
                /////////////////////////
                state.Current.Tick(time);
                ///////////////////////
                //////////////////////
                /////////////////////
                ////////////////////
                ///////////////////
                //////////////////
                /////////////////
                ////////////////
                ///////////////
                //////////////
                /////////////
                ////////////
                ///////////
                //////////
                /////////
                ////////   Хоть один
                ///////   треугольник
                //////   в этом круглом
                /////       Мире *-*
                ////
                ///
                //
                /////////////////////////
                window.Display();
            }
        }
    }
}