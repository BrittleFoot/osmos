﻿using System;
using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace Osmos
{
    internal class Spracle
    {
        private static Random rnd = new Random();
        private static int id;

        public readonly int Id;
        private readonly float rotsign;
        private float alpha;
        private Vector2f position;
        private float radius;
        private Vector2f speed;

        public Spracle(Vector2f pos, float rad, Vector2f speed = default(Vector2f))
        {
            Id = GetId();
            position = pos;
            radius = rad;
            alpha = 1;
            rotsign = (float) (rnd.NextDouble()*4 - 2);
            this.speed = speed;
            this.speed = new Vector2f(0.1f, 0.1f);
        }

        private Vector2f scale
        {
            get { return new Vector2f(1, 1)*(1f - alpha)*2; }
        }

        private float rot
        {
            get { return (1 - alpha)*90*rotsign; }
        }

        public bool IsDead
        {
            get { return alpha == 0; }
        }

        private static int GetId()
        {
            return id++;
        }

        public bool Update(float time)
        {
            alpha = Math.Max(0, alpha - 0.00001f*time);
            position += speed;
            speed = speed*(1 - 0.001f*time);
            return !IsDead;
        }

        public void Draw(RenderWindow window, CircleShape src)
        {
            src.Position = position;
            src.Radius = Math.Max(radius, 150);
            src.Origin = new Vector2f(src.Radius, src.Radius);
            var a = scale.X < 0.3f ? (byte) (scale.X*255) : (byte) (alpha*alpha*255);
            src.FillColor = new Color(255, 255, 255, a);
            src.Scale = scale;
            src.Rotation = rot;
            window.Draw(src);
        }
    }
}