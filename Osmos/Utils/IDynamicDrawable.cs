﻿using SFML.Graphics;

namespace Osmos.Utils
{
    public interface IDynamicDrawable : IDynamic
    {
        void Draw(RenderWindow window);
    }
}