﻿namespace Osmos.Utils
{
    public interface IGameState
    {
        void Tick(float time);
    }
}