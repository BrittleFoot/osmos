using SFML.Graphics;
using SFML.Window;

namespace Osmos.Utils
{
    public static class RenderWindowExtentions
    {
        public static VideoMode GetVMode(this RenderWindow window)
        {
            return new VideoMode(window.Size.X, window.Size.Y);
        }
    }
}